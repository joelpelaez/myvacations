<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/places', 'PlaceController@getAllPlaces');
Route::get('/places/{id}', 'PlaceController@getPlaceById');
Route::get('/places/{place_id}/visits', 'PlaceController@getAllVisitPlaces');
Route::get('/places/{place_id}/visits/{id}', 'PlaceController@getVisitPlacesById');