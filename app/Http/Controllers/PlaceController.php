<?php

namespace App\Http\Controllers;

use App\Place;
use App\VisitPlaces;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Psy\Util\Json;

class PlaceController extends Controller
{
    /**
     *
     */
    public function getAllPlaces(Request $request) {
        $places = Place::all();

        return JsonResponse::create($places);
    }

    public function getPlaceById(Request $request, string $id) {
        $place = Place::query()->find($id);

        return JsonResponse::create($place);
    }

    public function getAllVisitPlaces(Request $request, string $place_id) {
        $visit_places = VisitPlaces::query()->where('place_id', $place_id)->get();
        return JsonResponse::create($visit_places);
    }

    public function getVisitPlacesById(Request $request, string $place_id, string $id) {
        $visit_place = VisitPlaces::query()->whereKey($id)->where('place_id', $place_id)->get();
        return JsonResponse::create($visit_place);
    }
}
