<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaceStatus extends Model
{
    protected $table = 'places_status';
    protected $primaryKey = 'id';
    //
}
