<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitPlaces extends Model
{
    protected $table = 'visit_places';
    protected $primaryKey = 'id';
    //
}
